# Project Library
## _TP Final - Desarrollo de Aplicaciones Web_


### Proyecto Administración de una biblioteca escolar. 

## Integrantes
- Hendel, Florencia.
- Riverol, Daniel.
## Docente
- Pablo Ingino

## Características
- Sesión por Roles -> Administrador, Docente, Alumno.
- Password encriptado.
- Alta, baja y modificación de libros.
- Visualización de catálogo de libros.
- Préstamo de libros.
- Devolución de libros.
- Visualización listado de usuarios.

## Tecnologías

Desarrollado con las siguientes tecnologías: 

- [PHP] - Backend
- [MySQL] - Base de datos
- [Materialize CSS](https://materializecss) - Frontend.

## Configurar setup
- Crear base de datos library-mvc
- Ver configuración en Controllers/connection.php
- Importar archivo library-mvc.sql a la BD.

### ROLES

- Administrador 
- Docente
- Alumno 

### User/Pass
- ADMINISTRADOR => admin | admin123
- DOCENTE => luciasuarez | 12345
- ALUMNOS => luisperez | 12345 , briantorres | 12345 , marialujan | 12345


